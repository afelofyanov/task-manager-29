package ru.tsc.felofyanov.tm.exception.system;

import ru.tsc.felofyanov.tm.exception.AbstractExcception;

public final class ArgumentNotSupportedException extends AbstractExcception {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(String message) {
        super("Error! Argument [" + message + "] not supported...");
    }
}
